import Paul  from './imgs/Paul.jpg';
import Vianney from './imgs/Vianney.jpg';
import Sting  from './imgs/Sting.jpg';
import Queen  from './imgs/Queen.png';
import ACDC  from './imgs/ACDC.jpg';
import Phil from './imgs/Phil.jpg';
import Jean from './imgs/Jean.jpg';
import Serge from './imgs/Serge.jpg';
import son_acdc from './sounds/acdc.mp3';
import son_collins from './sounds/collins.mp3';
import son_goldman from './sounds/goldman.mp3';
import son_paul from './sounds/paul.mp3';
import son_queen from './sounds/queen.mp3';
import son_serge from './sounds/serge.mp3';
import son_sting from './sounds/sting.mp3';
import son_vianney from './sounds/vianney.mp3';


export default [
    {
        title: "Paul Personne",
        content: "Auteur-compositeur-interprète français",
        image: Paul,
        sound: son_paul,
    },
    {
        title: "Vianney Bureau",
        content: "Auteur-compositeur-interprète français",
        image: Vianney,
        sound: son_vianney,
    },
    {
        title: "Sting",
        content: "Auteur-compositeur-interprète et musicien britannique.",
        image: Sting,
        sound: son_sting,
    },
    {
        title: "Queen",
        content: "Groupe de rock britannique, originaire de Londres, en Angleterre.",
        image: Queen,
        sound: son_queen,
    },
    {
        title: "AC/DC",
        content: "Groupe de hard rock australo-britannique, originaire de Sydney.",
        image: ACDC,
        sound: son_acdc,
    },
    {
        title: "Phil Collins",
        content: "Auteur-compositeur-interprète, acteur et producteur de disques britannique.",
        image: Phil,
        sound: son_collins,
    },
    {
        title: "Jean-Jacques Goldman",
        content: "Auteur-compositeur-interprète français, producteur et guitariste soliste",
        image: Jean,
        sound: son_goldman,
    },
    {
        title: "Serge Gainsbourg",
        content: "auteur-compositeur-interprète français",
        image: Serge,
        sound: son_serge,
    },
    ]