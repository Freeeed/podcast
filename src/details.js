import {FaBeer} from 'react-icons/fa';
import {FaStar}	from 'react-icons/fa';
import {BiStar}	from 'react-icons/bi';


export default function Details(props) {
    console.log(props.itemPara)
    return(
        <div className="divDetails">
            <h2>{props.itemPara.title} 
            { props.itemPara ?  <BiStar /> : <FaStar color="yellow"/>} </h2>
            <p>{props.itemPara.details}</p>
            <p>{props.itemPara.content}</p>
            <figure>
                <audio controls src={props.itemPara.sound}>
                </audio>    
            </figure>            
            <img id="imagedudetail" src={props.itemPara.image} /> 
            <p>{props.itemPara.date}</p>
        </div>
    )
}