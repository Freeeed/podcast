import {
    BrowserRouter as Router,
    Switch,
    Route,
    useHistory,
    Link
  } from "react-router-dom";
import './objectTable';
import objectTable from './objectTable';
import Podcast from './podcast';
import Details from './details';
import React, { useEffect, useState } from 'react';

export default function Liste() {
    const [itemPara, setItemPara] = useState();
    const [idOfDetail, setIdOfDetail] = useState("1");
    const [nomRecherche, setNomRecherche] = useState('');
    const [statePod, setStatePod] = useState(objectTable);
    const [descOrName, setDescOrName] = useState('title')
    const history = useHistory();
    const handleChange = e => {
      setNomRecherche(e.target.value);
    }
    function handleClick() {
    history.push("/");
    }

    function takeClickedItem(itempara) {
        setItemPara(itempara);
        console.log(itemPara)   
    }

    return <div>

            <div className="searchbar">
                <div className="searchbar_input">
                    <input type="text" value={nomRecherche} onChange={handleChange} placeholder="Rechercher..."/>  
                </div>
                <div classname="searchbar_radio">
                        <label>RECHERCHE PAR NOM</label>
                        <input name="changefilter" type="radio" onChange={() => setDescOrName('title')}/>    

                        <label>RECHERCHE PAR DESCRIPTION</label>
                        <input name="changefilter" type="radio"onChange={() => setDescOrName('content')}/>       
                </div>
            </div>

            <div className="allthepod">
                {statePod.filter(object => object[descOrName].toLowerCase().includes(nomRecherche.toLowerCase())).map((item) => {
                    return (
                        <Podcast className="podcast_single"
                        title={item.title}
                        item={item}
                        takeClickedItem={takeClickedItem}
                        image={item.image}
                        />
                    );
                    })}
            </div>

            <div id="basdepage">
               {
                   itemPara && <Details
                        itemPara={itemPara}
                    />
               }
            </div>
    </div>
  }