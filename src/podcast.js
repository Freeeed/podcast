import {FaBeer} from 'react-icons/fa';
import {FaStar}	from 'react-icons/fa';
import {BiStar}	from 'react-icons/bi';


export default function Podcast(props) {
    return (
        <a className="podcast" href="#basdepage" onClick={() => props.takeClickedItem(props.item)}>
        <div className="podcast">
            <div className="podimage">
               <img src={props.image} />  
            </div>
            <div className="podtext">
            <h2>{props.title}</h2>            
            </div>
        </div>
        </a>
    )
}