import './App.css';
import React, { useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useHistory,
  Link
} from "react-router-dom";

import 'react-icons';
import Liste from "./Liste"
import {FaBeer} from 'react-icons/fa';
import {FaStar}	from 'react-icons/fa';
import {BiStar}	from 'react-icons/bi';
import Casque2 from './imgs/casque2.jpg';

export default function App() {

  return (
    <div className="App">
      <main className="page">
        <Router>

          <header>
            <div className="titlepage">
              <h1>LES POD POD</h1>
              <img className="imagecasque" src={Casque2} alt="casque"/>
            </div>
            <nav>
              <ul>
                <li>
                  <Link to="/">Accueil</Link>
                </li>
                <li>
                  <Link to="/Liste">Liste des Podcasts</Link>
                </li>
                <li>
                  <Link to="/Favoris">Vos Favoris</Link>
                </li>
              </ul>
            </nav>
          </header>

          <section className="sectionprincipale">
            <Switch>

              <Route path="/Liste">
                <Liste />
              </Route>

              <Route exact path="/">
                <Accueil />
              </Route>

              <Route path="/Favoris">
                <Favoris />
              </Route>
              
            </Switch>    

                 
          </section>

        </Router>
      </main>
    </div>
  );
}

function Accueil() {
  return <div className="texte"><h2>Accueil</h2><h4> Bienvenue sur les Pod Pod </h4> 
            <p>Vous trouverez ici des podcasts sur plusieurs de nos plus grands artistes.</p> 
            <p> Bonne écoute !</p> 
           
  </div>;


  

}

function Favoris() {
  const history = useHistory();
  
    function handleClick() {
      history.push("/");
    }
  return <div>
  <h2>Vos Favoris</h2>
  <button type="button" onClick={handleClick}> Go home </button>
  
  </div>
}